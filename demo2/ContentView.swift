//
//  ContentView.swift
//  demo2
//
//  Created by Marius Bologa on 10/3/19.
//  Copyright © 2019 Marius Bologa. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var sortOption = true
    @State private var menu = loadJson(filename: "data")
    
    var body: some View {
        let categories = menu.keys.sorted()
        let items = menu.sorted{ $0.key < $1.key }.map { $0.value.sorted(by: {
            guard let firstName = $0.name, let secondName = $1.name else { return false }
            return self.sortOption ?firstName < secondName : firstName > secondName}) }
        return NavigationView {
            List {
                ForEach(categories.indices) {index in
                    Section(header: Text(categories[index])) {
                        ForEach(items[index]) { item in
                            Text(item.name ?? "")
                        }
                    }
                }
            }.navigationBarTitle("Menu").listStyle(GroupedListStyle())
                .navigationBarItems(trailing:
                    Button(action: {
                        self.sortOption.toggle()
                    }) {
                        Text(sortOption ? "Z -> A" : "A -> Z")
                })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
