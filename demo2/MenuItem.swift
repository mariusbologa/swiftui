//
//  MenuItem.swift
//  demo2
//
//  Created by Marius Bologa on 10/3/19.
//  Copyright © 2019 Marius Bologa. All rights reserved.
//

import Foundation

struct MenuItem: Decodable, Identifiable {
    var id: UUID?
    var name: String?
    var price: Double?
    var consumable: String?
    var cuisine_name: String?
    var category_name: String?
    
    init(id: UUID?, name: String?, price: Double?, consumable: String?, cuisine_name: String?, category_name: String?) {
        self.id = id
        self.name = name
        self.consumable = name
        self.price = price
        self.cuisine_name = cuisine_name
        self.category_name = category_name
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name, price, consumable, cuisine_name, category_name
    }
}
