//
//  Helper.swift
//  demo2
//
//  Created by Marius Bologa on 10/3/19.
//  Copyright © 2019 Marius Bologa. All rights reserved.
//

import Foundation


func loadJson(filename fileName: String)-> Dictionary<String, [MenuItem]> {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode(Menu.self, from: data)
            let items = filterAndGroupData(menuItems: jsonData.products)
            return items
        } catch {
            print("error:\(error)")
        }
    }
    return [:]
}


func filterAndGroupData(menuItems: [MenuItem]?)-> Dictionary<String, [MenuItem]> {
    guard let items = menuItems else { return [:] }
    let validItems = items.filter {
        guard let name = $0.name else { return false }
        return !name.isEmpty }
    return Dictionary(grouping: validItems, by: {
        guard let category_name = $0.category_name else { return "none" }
        return category_name })
}
