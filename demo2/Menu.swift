//
//  Menu.swift
//  demo2
//
//  Created by Marius Bologa on 10/3/19.
//  Copyright © 2019 Marius Bologa. All rights reserved.
//

import Foundation

struct Menu: Decodable {
    var products: [MenuItem]?
    
    init(products: [MenuItem]) {
        self.products = products
    }
    
    enum CodingKeys: String, CodingKey {
        case products
    }
}
